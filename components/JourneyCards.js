import React from "react";
import Link from "next/link";

export const JourneyCard = ({ day, description, disabled, link }) => {
  if (disabled === true) {
    return (
      <a
        href="#"
        className="bg-slate-200 w-80 lg:w-96 flex flex-col p-4 rounded-md opacity-60 transition-opacity cursor-not-allowed mb-4"
      >
        {/* Current day */}
        <span className="text-slate-600">Day {day}</span>

        {/* Task for the day */}
        <p className="text-slate-700 mt-2">{description}</p>

        {/* Click to see more */}
        <span className="text-slate-500 mt-4">Click to see more...</span>
      </a>
    );
  } else {
    return (
      <a
        href={link}
        
      >
        <div className="bg-slate-200 w-80 lg:w-96 flex flex-col p-4 rounded-md hover:opacity-80 transition-opacity cursor-pointer mb-4">
          {/* Current day */}
          <span className="text-slate-600">Day {day}</span>

          {/* Task for the day */}
          <p className="text-slate-700 mt-2">{description}</p>

          {/* Click to see more */}
          <span className="text-slate-500 mt-4">Click to see more...</span>
        </div>
      </a>
    );
  }
};
