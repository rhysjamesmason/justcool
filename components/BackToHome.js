import Head from "next/head";
import Link from "next/link";
import React from "react";

export const BackToHome = ({ day, shortDesc }) => {
  return (
    <div className="bg-slate-200 w-screen h-1/4 flex flex-row p-4">
      <Head>
        {/* <!-- Primary Meta Tags --> */}
        <title>Day {day} - Just Cool NCS</title>
        <meta name="title" content={`Day ${ day } - Just Cool NCS`} />
        <meta name="description" content={shortDesc} />

        {/* <!-- Open Graph / Facebook --> */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content={`https://justcoolncs.tk/connect/day-${day}`} />
        <meta property="og:title" content={`Day ${ day } - Just Cool NCS`} />
        <meta property="og:description" content={shortDesc} />
        <meta property="og:image" content={`https://cdn.statically.io/og/Day%20${day}.webp`} />

        {/* <!-- Twitter --> */}
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content={`https://justcoolncs.tk/connect/day-${day}`} />
        <meta property="twitter:title" content={`Day ${ day } - Just Cool NCS`} />
        <meta property="twitter:description" content={shortDesc} />
        <meta property="twitter:image" content={`https://cdn.statically.io/og/Day%20${day}.webp`} />
      </Head>
      <a href={`/#journey`}>
        <div className="w-4/5">
          <svg
            className="w-6 h-6 cursor-pointer"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M7.707 14.707a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 1.414L5.414 9H17a1 1 0 110 2H5.414l2.293 2.293a1 1 0 010 1.414z"
              clipRule="evenodd"
            />
          </svg>
        </div>
      </a>
      <div className="">
        <span className="w-1/5 font-black">Day {day}</span>
      </div>
    </div>
  );
};
