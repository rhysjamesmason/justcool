import Link from "next/link";
import React from "react";

export const FooterComponent = (props) => {
  return (
    <footer className="bg-slate-900 text-white  w-full">
      <div className="flex flex-col lg:flex-row justify-center items-center p-4">
        {/* Information */}
        <div className="text-center lg:w-1/2">
            <h1>Project for <strong className="font-black">NCS</strong></h1>
            <p>For Group 5, 2022</p>
        </div>

        {/* Site Links */}
        <div className="text-center mt-12 underline lg:w-1/2">
            <ul>
                <li><Link href={'/'}>Home</Link></li>
                <li><Link href={'/connect/day-one'}>Day One</Link></li>
                <li><Link href={'/connect/day-two'}>Day Two</Link></li>
                <li><Link href={'/connect/day-three'}>Day Three</Link></li>
                <li><Link href={'/connect/day-four'}>Day Four</Link></li>
                <li><Link href={'/connect/day-five'}>Day Five</Link></li>
            </ul>
        </div>
      </div>
    </footer>
  );
};
