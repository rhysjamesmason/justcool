import React from "react";
import Image from "next/image";

export const ResultSection = (props) => {
  return (
    <section className="bg-red-500 h-screen flex flex-col justify-center items-center">
      <span className="text-white text-4xl font-bold">The result:</span>

      {/* Flex Row Container */}
      <div className="flex flex-row">
        {/* Image */}
        <div className="p-8">
          <h3 className="text-white text-xl font-bold">Before</h3>
          <Image
            className="rounded-md shadow-lg bg-yellow-300 p-4"
            src={`https://ik.imagekit.io/rhysjames/justcoolncs_tk/IMG-20220727-WA0003_FrFpneVPX.jpg?ik-sdk-version=javascript-1.4.3&updatedAt=1659027407405`}
            alt="Before"
            height={500}
            width={500}
          />
        </div>
        <div className="p-8">
          <h3 className="text-white text-xl font-bold">After</h3>
          <Image
            loading="lazy"
            className="rounded-md shadow-lg bg-yellow-300 p-4"
            src={`https://ik.imagekit.io/rhysjames/justcoolncs_tk/after_LOqoEMtfz?ik-sdk-version=javascript-1.4.3&updatedAt=1659111365900`}
            alt="Before"
            height={500}
            width={500}
          />
        </div>

        {/* Description */}
        <div></div>
      </div>
    </section>
  );
};
