import React from "react";
import Script from "next/script";

export const AboutSection = (props) => {
  return (
    <section className="flex flex-col justify-center items-center lg:h-screen w-full">
      {/* Meet the team Title */}
      <span className="text-3xl font-bold">About us!</span>

      <div className="lg:flex lg:flex-row">
        {/* TikTok video embed */}
        <div className="lg:w-1/2">
          <blockquote
            className="tiktok-embed"
            cite="https://www.tiktok.com/@justcoolncs/video/7124247511186935046"
            data-video-id="7124247511186935046"
            style={{
              maxWidth: "605px",
              minWidth: "325px",
            }}
          >
            {" "}
            <section>
              {" "}
              <a
                target="_blank"
                title="@justcoolncs"
                href="https://www.tiktok.com/@justcoolncs"
                rel="noreferrer"
              >
                @justcoolncs
              </a>{" "}
              top qualities of{" "}
              <a
                title="justcool"
                target="_blank"
                href="https://www.tiktok.com/tag/justcool"
                rel="noreferrer"
              >
                #justcool
              </a>{" "}
              <a
                title="ncs"
                target="_blank"
                href="https://www.tiktok.com/tag/ncs"
                rel="noreferrer"
              >
                #ncs
              </a>{" "}
              <a
                title="famous"
                target="_blank"
                href="https://www.tiktok.com/tag/famous"
                rel="noreferrer"
              >
                #famous
              </a>{" "}
              <a
                title="fypシ"
                target="_blank"
                href="https://www.tiktok.com/tag/fyp%E3%82%B7"
                rel="noreferrer"
              >
                #fypシ
              </a>{" "}
              <a
                title="fyp"
                target="_blank"
                href="https://www.tiktok.com/tag/fyp"
                rel="noreferrer"
              >
                #fyp
              </a>{" "}
              <a
                target="_blank"
                title="♬ original sound - The nostalgia is real"
                href="https://www.tiktok.com/music/original-sound-7068743656957987590"
                rel="noreferrer"
              >
                ♬ original sound - The nostalgia is real
              </a>{" "}
            </section>{" "}
          </blockquote>{" "}
          <Script async src="https://www.tiktok.com/embed.js"></Script>
        </div>

        {/* A brief description about us */}
        <p className="mt-6 p-12 lg:w-1/2">
          We are Just Cool. Group 5 from NCS 2022 that choose the social action
          project of volunteering at a care home for the elderly. We choose this
          because it will have a greater impact for the older genration.
          <br />
          <br />
          We want older people to connect with their families, this is done
          buying running a workshop on how to use technology and how to prevent
          scams targeted to the unaware. This will also show how to spot the
          phishing attempts before it is to late.
        </p>
      </div>
    </section>
  );
};
