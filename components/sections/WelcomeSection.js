import React from "react";

export const WelcomeSection = (props) => {
  return (
    <main className="bg-slate-600 h-screen flex flex-col justify-center items-center w-full">
      <span className="text-2xl uppercase tracking-wide text-slate-200 font-bold">
        Welcome to,
      </span>
      <span className="text-5xl uppercase tracking-wide text-orange-300 font-black">
        Just Cool
      </span>
      <span className="text-2xl uppercase tracking-wide text-slate-200 font-bold">
        Our project is
      </span>
      <span className="text-5xl uppercase tracking-wide text-purple-300 font-black">
        Hillcare
      </span>

      <div className="h-44" />

      <svg
        className="w-8 h-8 text-white"
        fill="currentColor"
        viewBox="0 0 20 20"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          fillRule="evenodd"
          d="M15.707 4.293a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-5-5a1 1 0 011.414-1.414L10 8.586l4.293-4.293a1 1 0 011.414 0zm0 6a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-5-5a1 1 0 111.414-1.414L10 14.586l4.293-4.293a1 1 0 011.414 0z"
          clipRule="evenodd"
        />
      </svg>
    </main>
  );
};
