import React from "react";

export const SocialSection = (props) => {
  return (
    <section className="bg-indigo-500 flex flex-col justify-center items-center p-2">
      {/* Social Title */}
      <h1 className="text-2xl text-white font-bold mb-6">Our Socials</h1>

      {/* Social Links */}
      <a
        href="https://www.tiktok.com/@justcoolncs"
        className="bg-indigo-200 rounded-lg shadow-md h-2/5 w-full flex flex-row items-center justify-center p-6 cursor-pointer transition-opacity hover:opacity-80"
      >
        <div className="pr-4">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            style={{
              fill: "rgba(0, 0, 0, 1)",
            }}
          >
            <path d="M19.59 6.69a4.83 4.83 0 0 1-3.77-4.25V2h-3.45v13.67a2.89 2.89 0 0 1-5.2 1.74 2.89 2.89 0 0 1 2.31-4.64 2.93 2.93 0 0 1 .88.13V9.4a6.84 6.84 0 0 0-1-.05A6.33 6.33 0 0 0 5 20.1a6.34 6.34 0 0 0 10.86-4.43v-7a8.16 8.16 0 0 0 4.77 1.52v-3.4a4.85 4.85 0 0 1-1-.1z"></path>
          </svg>
        </div>
        <div>
          <h1 className="font-black text-xl">TikTok</h1>
        </div>
      </a>
      {/* Instagram */}
      <a
        href="https://www.instagram.com/justcoolncs/?igshid=YmMyMTA2M2Y%3D"
        className="bg-indigo-200 rounded-lg shadow-md h-2/5 w-full flex flex-row items-center justify-center p-6 mb-6 mt-4 cursor-pointer transition-opacity hover:opacity-80"
      >
        <div className="pr-4">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            style={{
              fill: "rgba(0, 0, 0, 1)",
            }}
          >
            <path d="M20.947 8.305a6.53 6.53 0 0 0-.419-2.216 4.61 4.61 0 0 0-2.633-2.633 6.606 6.606 0 0 0-2.186-.42c-.962-.043-1.267-.055-3.709-.055s-2.755 0-3.71.055a6.606 6.606 0 0 0-2.185.42 4.607 4.607 0 0 0-2.633 2.633 6.554 6.554 0 0 0-.419 2.185c-.043.963-.056 1.268-.056 3.71s0 2.754.056 3.71c.015.748.156 1.486.419 2.187a4.61 4.61 0 0 0 2.634 2.632 6.584 6.584 0 0 0 2.185.45c.963.043 1.268.056 3.71.056s2.755 0 3.71-.056a6.59 6.59 0 0 0 2.186-.419 4.615 4.615 0 0 0 2.633-2.633c.263-.7.404-1.438.419-2.187.043-.962.056-1.267.056-3.71-.002-2.442-.002-2.752-.058-3.709zm-8.953 8.297c-2.554 0-4.623-2.069-4.623-4.623s2.069-4.623 4.623-4.623a4.623 4.623 0 0 1 0 9.246zm4.807-8.339a1.077 1.077 0 0 1-1.078-1.078 1.077 1.077 0 1 1 2.155 0c0 .596-.482 1.078-1.077 1.078z"></path>
            <circle cx="11.994" cy="11.979" r="3.003"></circle>
          </svg>
        </div>
        <div>
          <h1 className="font-black text-xl">Instagram</h1>
        </div>
      </a>
    </section>
  );
};
