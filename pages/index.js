import Head from "next/head";
import Image from "next/image";
import Script from "next/script";
import { JourneyCard } from "../components/JourneyCards";
import { AboutSection } from "../components/sections/AboutSection";
import { ResultSection } from "../components/sections/AimsSection";
import { SocialSection } from "../components/sections/SocialSection";
import { WelcomeSection } from "../components/sections/WelcomeSection";

export default function Home() {
  return (
    <div className="w-screen mx-auto overflow-x-hidden">
      <Head>
        {/* <!-- Primary Meta Tags --> */}
        <title>Home - Just Cool NCS</title>
        <link rel="canonical" href="https://justcoolncs.tk/" />
        <meta name="title" content="Home - Just Cool NCS" />
        <meta
          name="description"
          content="This is our social project home page. On the page you can see our journey though out and find all our social media."
        />

        {/* <!-- Open Graph / Facebook --> */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://justcoolncs.tk/" />
        <meta property="og:title" content="Home - Just Cool NCS" />
        <meta
          property="og:description"
          content="This is our social project home page. On the page you can see our journey though out and find all our social media."
        />
        <meta property="og:image" content="https://cdn.statically.io/og/Home.webp" />

        {/* <!-- Twitter --> */}
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://justcoolncs.tk/" />
        <meta property="twitter:title" content="Home - Just Cool NCS" />
        <meta
          property="twitter:description"
          content="This is our social project home page. On the page you can see our journey though out and find all our social media."
        />
        <meta property="twitter:image" content="https://cdn.statically.io/og/Home.webp" />
      </Head>
      <WelcomeSection />

      {/* Transition */}
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#475569"
          fillOpacity="1"
          d="M0,192L48,197.3C96,203,192,213,288,197.3C384,181,480,139,576,117.3C672,96,768,96,864,112C960,128,1056,160,1152,181.3C1248,203,1344,213,1392,218.7L1440,224L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"
        ></path>
      </svg>

      {/* About us! */}
      <AboutSection />

      {/* Transition */}
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#ef4444"
          fillOpacity="1"
          d="M0,32L48,37.3C96,43,192,53,288,80C384,107,480,149,576,165.3C672,181,768,171,864,181.3C960,192,1056,224,1152,240C1248,256,1344,256,1392,256L1440,256L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
        ></path>
      </svg>

      {/* The Result of the project */}
      <ResultSection />

      {/* Transition */}
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#ef4444"
          fillOpacity="1"
          d="M0,32L48,58.7C96,85,192,139,288,186.7C384,235,480,277,576,266.7C672,256,768,192,864,144C960,96,1056,64,1152,64C1248,64,1344,96,1392,112L1440,128L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"
        ></path>
      </svg>

      {/* Our Journey */}
      <section
        id="journey"
        className="flex flex-col lg:flex-row items-center justify-center"
      >
        <div className="lg:w-1/2 lg:flex lg:justify-center lg:items-center">
          <h1 className="text-2xl mt-4 font-bold">Our Journey</h1>
        </div>
        <div className="p-4 lg:flex lg:flex-col lg:justify-center lg:items-center lg:w-1/2">
          <JourneyCard
            link={"/connect/day-one"}
            day={1}
            description="Monday we're planning out and pitching the ideas."
            disabled={false}
          />
          <JourneyCard
            link={"/connect/day-two"}
            day={2}
            description="Tuesday we're doing a sponsored walk to fundraise for the paints and other tools needed."
            disabled={false}
          />
          <JourneyCard
            link={"/connect/day-three"}
            day={3}
            description="Wednesday we're buying the paint and tools, then painting the wall."
            disabled={false}
          />
          <JourneyCard
            link={"/connect/day-four"}
            day={4}
            description="Thursday we're finishing off the wall and other activities that maybe happening."
            disabled={false}
          />
          <JourneyCard
            link={"/connect/day-five"}
            day={5}
            description="Friday is the Barnsley Challange and the Lock Park Party."
            disabled={true}
          />
        </div>
      </section>

      {/* Transition */}
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#6366f1"
          fillOpacity="1"
          d="M0,192L48,181.3C96,171,192,149,288,160C384,171,480,213,576,197.3C672,181,768,107,864,96C960,85,1056,139,1152,144C1248,149,1344,107,1392,85.3L1440,64L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
        ></path>
      </svg>

      {/* Social Media */}
      <SocialSection />
    </div>
  );
}
