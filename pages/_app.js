import Head from "next/head";
import Script from "next/script";
import { FooterComponent } from "../components/Footer";
import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <link
          rel="sitemap"
          type="application/xml"
          title="Sitemap"
          href="https://justcoolncs.tk/sitemap.xml"
        />
        {/* <meta httpEquiv="Content-Security-Policy" content="script-src 'none'"></meta> */}
      </Head>
      <Component {...pageProps} />
      <FooterComponent />
      <Script
        src="//gc.zgo.at/count.js"
        data-goatcounter="https://stats.justcoolncs.tk/count"
        async
      ></Script>
    </>
  );
}

export default MyApp;
