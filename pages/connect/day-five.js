import React from "react";
import Image from "next/image";
import { BackToHome } from "../../components/BackToHome";
import Head from "next/head";

export default function DayThree() {
  return (
    <div className="w-screen overflow-x-hidden">
      <BackToHome
        day={"five"}
        shortDesc="Friday is the Barnsley Challange and the Lock Park Party."
      />
      <Head>
        <link rel="canonical" href="https://justcoolncs.tk/connect/day-five" />
      </Head>

      {/* Main Article */}
      <main className="flex flex-col justify-center items-center p-6">

      </main>
    </div>
  );
}
