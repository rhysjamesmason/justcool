import React from "react";
import Image from "next/image";
import { BackToHome } from "../../components/BackToHome";
import Head from "next/head";

export default function DayThree() {
  return (
    <div className="w-screen overflow-x-hidden">
      <BackToHome
        day={"four"}
        shortDesc="Thursday we're finishing off the wall and other activities that maybe happening."
      />
      <Head>
        <link rel="canonical" href="https://justcoolncs.tk/connect/day-four" />
      </Head>

      {/* Main Article */}
      <main className="flex flex-col justify-center items-center p-6">
        Today we met at McDonalds since we needed to go to the <strong>Asda</strong> to get
        some flowers, prizes and bunting for the garden, however the prizes was
        for the bingo coffee morning for the residents. We picked some nice
        contrasting flowers and grown plants for the garden to make it look nice
        and have some nice vegetables. After we finished at Asda we went to
        Wickes to get some more of the paint needed to finish off the fence, we
        also got some gloves for the gardening aspect of the project.
        <br /> <br />
        <strong>After we finishing the shopping</strong>, we caught the number 1 bus to get
        to the care home with all our flowers and paint. It was about a 7 minute
        trip, when we got there we got started immediately. <strong>2 people</strong> did the
        coffee morning with the residents, whilst <strong>6 people</strong> added another
        coat of paint to the fence and preparing the vegetable patches and
        flower patches. Furthermore, we were told that people from NCS was
        checking our progress with the project.
        <br />
        <br />
        We finished the project by decorating the fence with bubbles and
        flowers, with also having a flower that had hand prints as the leaves
        from everyone in the group, <strong>except the group leaders</strong>. Finally, we
        said goodbye to the residents & staff and headed home.
      </main>
    </div>
  );
}
