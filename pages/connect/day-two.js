import React from "react";
import Image from "next/image";
import { BackToHome } from "../../components/BackToHome";
import Head from "next/head";

export default function DayTwo() {
  return (
    <div className="w-screen overflow-x-hidden">
      <BackToHome day={'two'} shortDesc="Tuesday we're doing a sponsored walk to fundraise for the paints and other tools needed." />

      <Head>
        <link rel="canonical" href="https://justcoolncs.tk/connect/day-two" />
      </Head>

      {/* Main Article */}
      <main className="flex flex-col justify-center items-center p-6">
        <h1 className="text-3xl" id="sponsored-walk">Sponsored walk</h1><br></br>
        <p>
          We started our walk at the <strong>Barnsley Interchange</strong> and
          walked down to the Asda on Old Mill Lane. We got supplies for our park
          picnic and started to head off to Mapplewell.{" "}
        </p>
        <p>
          When we arrived at Mapplewell park, we had a rest for an hour and had
          our lunches and took an amazing photo of the group, sadly we had a
          person off sick.
        </p>
        <p>
          After our break we set off again towards Darton park where we had
          another rest since we were still tired.
        </p><br/>
        <h2 className="text-2xl" id="final-stretch">Final Stretch</h2><br />
        <p>
          The final stretch is from Darton park to the Barnsley Interchange,
          this was long and so tiring to walk. However, we persevered and got to
          our destination.
        </p>
      </main>
    </div>
  );
}
