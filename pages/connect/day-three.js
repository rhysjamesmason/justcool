import React from "react";
import Image from "next/image";
import { BackToHome } from "../../components/BackToHome";
import Head from "next/head";

export default function DayThree() {
  return (
    <div className="w-screen overflow-x-hidden">
      <BackToHome
        day={"three"}
        shortDesc="Wednesday we're buying the paint and tools, then painting the wall."
      />
      <Head>
        <link rel="canonical" href="https://justcoolncs.tk/connect/day-three" />
      </Head>

      {/* Main Article */}
      <main className="flex flex-col justify-center items-center p-6">
        <p>
          We met at the interchange at 9 AM. Then we set off to Morrisons to get
          some food & snacks for the day. Today was about shopping and starting
          to paint the fence for the care home. So we looked into Wilkos for
          exterior paint, but it was too expensive, so we got rollers, paint
          trays and brushes (in a pack). We bought grey because they didn&apos;t have
          the colour we wanted for the fence, but we walked through to Wickes to
          buy the paint.
          <br />
          <br />
          The exact paint we bought was Cuprinol 5 year Ducksback Matt Shed &
          Fence Treatment - Silver Copes 9L. The product code is 173719 or the
          direct link to Wickes site is
          <a href="https://www.wickes.co.uk/Cuprinol-5-Year-Ducksback-Matt-Shed+Fence-Treatment---Silver-Copse-9L/p/173719">
            https://www.wickes.co.uk/Cuprinol-5-Year-Ducksback-Matt-Shed+Fence-Treatment---Silver-Copse-9L/p/173719
          </a>.
          <br />
          <br />
          After that, we caught the number 1 bus to the care home, and signed in
          at the reception. Before entering, we wore masks and took Covid tests
          to ensure the safety of the occupants. When in we met some people
          living there, then got to work painting and removing unwanted items.
          This took us the rest of the day. Then we had reflection and went
          home.
        </p>
      </main>
    </div>
  );
}
