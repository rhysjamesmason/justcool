import React from "react";
import Image from "next/image";
import { BackToHome } from "../../components/BackToHome";
import Head from "next/head";

export default function DayOne() {
  return (
    <div className="w-screen overflow-x-hidden">
      <BackToHome
        day={"one"}
        shortDesc="Monday we're planning out and pitching the ideas."
      />

      <Head>
        <link rel="canonical" href="https://justcoolncs.tk/connect/day-one" />
      </Head>

      {/* Main Article */}
      <main className="flex flex-col justify-center items-center p-6">
        <p className=" text-xl">
          At 9 am, we started to recap what we had previously discussed at the
          residential trip. Then we elected a team leader/project manager that
          delegated roles to everyone to work efficiently as a team. Two of our
          group contacted the care home, whilst another two people were tasked
          to add up the costs of getting paint. ‭Another‭ ‬two people were
          needed as social media managers, then the final two to fill out the
          project portfolio, which had to be done. We worked on this for an hour
          (s) until we took a break and filmed a TikTok that was showing the
          team. This was a great, since it was like a reset switch had just been
          pressed, and we could go back to our roles with a fresh set of eyes.
          <br />
          <br />
          Then at lunch the group, except the group leaders from NCS, set off to
          McDonald&apos;s to grab a some lunch and have a talk with the people
          in the group. Furthermore, we took selfies as a group and bought weird
          strawberries from a market stall.
          <br />
          <br />
          In the afternoon, we started to prepare for the pitch section of the
          day and started to make scripts for it. Then we rehearsed our pitch,
          then chilled for about 5 minutes. Our group was the last to show our
          pitch, and it went sort of smoothly, but could be better. After that,
          we got given our NCS t-shirts and started to head home.
        </p>
      </main>
    </div>
  );
}
